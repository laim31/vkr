from flask import Flask, render_template, request
import numpy as np
import pickle
import tensorflow as tf

app = Flask(__name__)

# Загрузка сохраненных моделей
with open("best_model.pkl", "rb") as file:
    best_model = pickle.load(file)

nn_model = tf.keras.models.load_model("nn_model.h5")

# Список столбцов для нейронной сети и машинного обучения
nn_columns = [
    'Плотность, кг/м3',
    'модуль упругости, ГПа',
    'Количество отвердителя, м.%',
    'Содержание эпоксидных групп,%_2',
    'Температура вспышки, С_2',
    'Поверхностная плотность, г/м2',
    'Модуль упругости при растяжении, ГПа',
    'Прочность при растяжении, МПа',
    'Потребление смолы, г/м2',
    'Угол нашивки, град',
    'Шаг нашивки',
    'Плотность нашивки'
]

ml_columns = [
    'Соотношение матрица-наполнитель',
    'Плотность, кг/м3',
    'модуль упругости, ГПа',
    'Количество отвердителя, м.%',
    'Содержание эпоксидных групп,%_2',
    'Температура вспышки, С_2',
    'Поверхностная плотность, г/м2',
    'Потребление смолы, г/м2',
    'Угол нашивки, град',
    'Шаг нашивки',
    'Плотность нашивки'
]

@app.route('/')
def index():
    return render_template('index.html', ml_columns=ml_columns, nn_columns=nn_columns)

@app.route('/ml', methods=['POST'])
def predict_ml():
    input_data = {key: float(request.form[key]) for key in ml_columns}
    input_array = np.array(list(input_data.values())).reshape(1, -1)
    ml_prediction = best_model.predict(input_array)
    elastic_modulus = ml_prediction[0][0]
    tensile_strength = round(ml_prediction[0][1], 2)
    return render_template('index.html', ml_columns=ml_columns, nn_columns=nn_columns, elastic_modulus=elastic_modulus, tensile_strength=tensile_strength)

@app.route('/nn', methods=['POST'])
def predict_nn():
    try:
        input_data = {key: float(request.form[key]) for key in nn_columns}
        input_array = np.array(list(input_data.values())).reshape(1, -1)
        nn_prediction = nn_model.predict(input_array)
        return render_template('index.html', ml_columns=ml_columns, nn_columns=nn_columns, nn_prediction=nn_prediction[0][0])
    except Exception as e:
        logging.exception(e)
        return str(e)
if __name__ == '__main__':
    app.run(debug=True)
