import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import StandardScaler
import pickle
import tkinter as tk
from tkinter import messagebox

# Загрузка датасетов
dataset_1 = pd.read_excel("X_bp.xlsx", index_col=0)
dataset_2 = pd.read_excel("X_nup.xlsx", index_col=0)

# Объединение датасетов по индексу с типом объединения INNER
merged_dataset = dataset_1.join(dataset_2, how="inner")

# 1. Удаление дубликатов
normalized_dataset = merged_dataset.drop_duplicates()

# 2. Обработка пропущенных значений
normalized_dataset = normalized_dataset.dropna()

#merged_dataset = merged_dataset.fillna(merged_dataset.mean())

# 3. Удаление выбросов (шумов) с использованием межквартильного размаха (IQR)
Q1 = normalized_dataset.quantile(0.25)
Q3 = normalized_dataset.quantile(0.75)
IQR = Q3 - Q1
normalized_dataset = normalized_dataset[~((normalized_dataset < (Q1 - 1.5 * IQR)) | (normalized_dataset > (Q3 + 1.5 * IQR))).any(axis=1)]

# 4. Нормализация данных с использованием мин-макс нормализации
#normalized_dataset = (merged_dataset - merged_dataset.min()) / (merged_dataset.max() - merged_dataset.min())

#5. Нормализация данных с использованием стандартизации
#scaler = StandardScaler()
#normalized_dataset = pd.DataFrame(scaler.fit_transform(normalized_dataset), columns=normalized_dataset.columns)


print(normalized_dataset)

# Разделение датасета на X (признаки) и y (целевые значения)
X = normalized_dataset.drop(columns=['Модуль упругости при растяжении, ГПа', 'Прочность при растяжении, МПа'])
y = normalized_dataset[['Модуль упругости при растяжении, ГПа', 'Прочность при растяжении, МПа']]

# Разделение данных на обучающую и тестовую выборки (70% обучение, 30% тест)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Создание модели RandomForestRegressor
model = RandomForestRegressor()

# Определение сетки гиперпараметров для поиска
param_grid = {
    'n_estimators': [10, 50, 100, 200],
    'max_depth': [None, 10, 20, 30],
    'min_samples_split': [2, 5, 10],
    'min_samples_leaf': [1, 2, 4]
}

# Поиск оптимальных гиперпараметров с использованием GridSearchCV и кросс-валидации (10 блоков)
grid_search = GridSearchCV(estimator=model, param_grid=param_grid, cv=10, scoring='neg_mean_squared_error', verbose=2, n_jobs=-1)
grid_search.fit(X_train, y_train)

# Обучение модели с оптимальными гиперпараметрами
best_model = grid_search.best_estimator_
best_model.fit(X_train, y_train)

# Сохранение best_model (RandomForestRegressor) в файле best_model.pkl
with open("best_model.pkl", "wb") as file:
    pickle.dump(best_model, file)

# Предсказание значений на тестовой выборке
y_pred = best_model.predict(X_test)

# Оценка качества модели (MSE и R^2)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)

print("Mean Squared Error:", mse)
print("R^2 Score:", r2)

# Разделение датасета на X (признаки) и y (целевые значения - соотношение матрица-наполнитель)
X = normalized_dataset.drop(columns=['Соотношение матрица-наполнитель'])
y = normalized_dataset['Соотношение матрица-наполнитель']

# Разделение данных на обучающую и тестовую выборки (70% обучение, 30% тест)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Создание нейронной сети
model = tf.keras.Sequential([
    tf.keras.layers.Dense(128, activation='relu', input_shape=(X_train.shape[1],)),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(1)
])

# Компиляция модели
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.005), loss='mse', metrics=['mae'])

# Обучение модели
history = model.fit(X_train, y_train, epochs=100, batch_size=32, validation_split=0.2, verbose=1)

# Сохранение модели нейронной сети в файле nn_model.h5
model.save("nn_model.h5")

# Предсказание значений на тестовой выборке
y_pred = model.predict(X_test)

# Оценка качества модели (MSE и R^2)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)

print("Mean Squared Error:", mse)
print("R^2 Score:", r2)

# Функция для предсказания значений с помощью модели машинного обучения
def predict_values_ml():
    try:
        input_values = []
        for entry in ml_entries:
            input_values.append(float(entry.get()))

        input_array = np.array(input_values).reshape(1, -1)
        input_array = pd.DataFrame(input_array, columns=normalized_dataset.drop(columns=['Модуль упругости при растяжении, ГПа', 'Прочность при растяжении, МПа']).columns)
        ml_prediction = best_model.predict(input_array)

        ml_result.set(f"Модуль упругости при растяжении, ГПа: {ml_prediction[0][0]:.2f}\nПрочность при растяжении, МПа: {ml_prediction[0][1]:.2f}")



    except Exception as e:
        messagebox.showerror("Ошибка", f"Проверьте правильность введенных данных\nError: {str(e)}")

# Функция для предсказания значений с помощью нейронной сети
def predict_values_nn():
    try:
        input_values = []
        for entry in nn_entries:
            input_values.append(float(entry.get()))

        input_array = np.array(input_values).reshape(1, -1)
        nn_prediction = model.predict(input_array)

        nn_result.set(f"Соотношение матрица-наполнитель: {nn_prediction[0][0]:.2f}")

    except Exception as e:
        messagebox.showerror("Ошибка", f"Проверьте правильность введенных данных\nError: {str(e)}")

# Создание графического интерфейса
app = tk.Tk()
app.title("Прогнозирование")
ml_label = tk.Label(app, text="Модель машинного обучения", font=("Arial", 16), fg='red')
ml_label.grid(row=0, column=0, columnspan=2, pady=(0, 10))

# Создание меток и полей ввода для признаков модели машинного обучения
ml_entries = []
ml_columns = normalized_dataset.drop(columns=['Модуль упругости при растяжении, ГПа', 'Прочность при растяжении, МПа']).columns

for i, column in enumerate(ml_columns):
    tk.Label(app, text=column).grid(row=i + 1, column=0)
    entry = tk.Entry(app)
    entry.grid(row=i + 1, column=1)
    ml_entries.append(entry)

# Создание меток и полей ввода для признаков нейронной сети
nn_label = tk.Label(app, text="Нейронная сеть", font=("Arial", 16), fg='red')
nn_label.grid(row=0, column=2, columnspan=2, pady=(0, 10))
nn_entries = []
nn_columns = normalized_dataset.drop(columns=['Соотношение матрица-наполнитель']).columns

for i, column in enumerate(nn_columns):
    tk.Label(app, text=column).grid(row=i+1, column=2)
    entry = tk.Entry(app)
    entry.grid(row=i+1, column=3)
    nn_entries.append(entry)

# Кнопка для предсказания с помощью модели машинного обучения
predict_button_ml = tk.Button(app, text="Спрогнозировать", command=predict_values_ml)
predict_button_ml.grid(row=len(ml_columns) + 3, columnspan=2)

# Кнопка для предсказания с помощью нейронной сети
predict_button_nn = tk.Button(app, text="Спрогнозировать", command=predict_values_nn)
predict_button_nn.grid(row=len(nn_columns) + 2 , column=2, columnspan=2)

# Метки для вывода результатов
ml_result = tk.StringVar()
nn_result = tk.StringVar()

tk.Label(app, textvariable=ml_result).grid(row=len(ml_columns) + 1, columnspan=2)
tk.Label(app, textvariable=nn_result).grid(row=len(nn_columns) + 1, column=2, columnspan=2)

# Запуск приложения
app.mainloop()